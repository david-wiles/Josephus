package edu.Josephus;

import perm.Permutation;
import perm.Permutator;
import perm.PingPongPermutator;
import perm.ResultMap;
import util.Utilities;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;


public class Main {

    public static void main(String[] args) {

//        try {
//            ResultMap results = callFunc(args);
//            results.saveToFile();
//        } catch (Exception ex) {
//            System.out.println(ex);
//        }

//        System.out.println(Statistics.inversionTable(Utilities.makeArray("163", 6))); // Prime
//        System.out.println(Statistics.inversionTable(Utilities.makeArray("178", 6))); // Factors: 2, 89
//        System.out.println(Statistics.inversionTable(Utilities.makeArray("541", 6))); // Prime
//        System.out.println(Statistics.inversionTable(Utilities.makeArray("556", 6))); // Factors: 2, 4, 139, 278

        Integer[] start = {1, 2, 3, 4, 5, 6};
        Permutator permutator = new PingPongPermutator(Arrays.asList(start));
        System.out.println(permutator.permutate(2).getPerm());
    }

    /**
     * Method which parses command line arguments and calls the proper function.
     *
     * @param args  The full list of command line arguments
     * @return      ResultMap from the result of the function call
     * @throws Exception    To be replaced with a more informative exception later
     */
    static ResultMap callFunc(String[] args) throws Exception {
        Class[] argType = new Class[args.length - 1];
        Object[] argsList = new Object[args.length - 1];

        for (int i = 1; i < args.length; ++i) {

            Object tmp;
            try {
                tmp = Integer.parseInt(args[i]);
            } catch (NumberFormatException ex) {
                tmp = args[i];
            }

            argType[i - 1] = tmp.getClass();
            argsList[i - 1] = tmp;
        }

        Method method = Main.class.getMethod(args[0], argType);
        return (ResultMap) method.invoke(null, argsList);
    }

}
