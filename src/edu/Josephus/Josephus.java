package edu.Josephus;

import perm.Permutation;
import perm.Permutator;
import perm.ResultMap;
import util.Utilities;

import java.util.ArrayList;
import java.util.Iterator;


public class Josephus {

    /**
     * Function which generates all of the possible first-order Josephus permutations from a
     * given start array.
     * @param list  Initial list to permutate
     * @return      ResultMap containing unique results.
     *              To get all possible results, use joeCounts (i.e. occurrence counts)
     */
    public static ResultMap joe(ArrayList<Integer> list) {
        ResultMap results = new ResultMap();
        Permutator p = new Permutator(list);
        Permutation res;

        int skip = 1;
        int lcm = Utilities.lcm(list.size()).intValue();
        while (skip < lcm) {
            res = p.permutate(skip++);
            results.add(res);
        }

        return results;
    }

    public static ResultMap joe(Permutator permutator) {
        ResultMap results = new ResultMap();
        Permutation res;

        int skip = 1;
        int lcm = Utilities.lcm(permutator.size()).intValue();
        while (skip < lcm) {
            res = permutator.permutate(skip++);
            results.add(res);
        }

        return results;
    }

    /**
     * Get all possible permutations of a certain order and return a ResultMap containing the results
     *
     * @param init  Array to start permutations with
     * @param order The number of times to recursively permutate
     * @return      ResultMap with all results
     */
    public static ResultMap joeRecursive(ArrayList<Integer> init, Integer order) {
        ResultMap startOrder = joe(init);

        for (int i = 0; i < order - 1; ++i) {
            ResultMap thisOrder = new ResultMap();
            Iterator<ResultMap.Result> itr = startOrder.iterator();

            // Iterate through permutations in initial set, and create a new set of permutations to add to results
            while (itr.hasNext()) {
                ResultMap next = joe(itr.next().permutation.getPerm());
                thisOrder.combine(next);
            }

            // Ressign start order for next permutation
            startOrder = thisOrder;
        }

        return startOrder;
    }

    public static ResultMap joeRecursive(Permutator permutator, Integer order) {
        ResultMap startOrder = joe(permutator);

        for (int i = 0; i < order - 1; ++i) {
            ResultMap thisOrder = new ResultMap();
            Iterator<ResultMap.Result> itr = startOrder.iterator();

            // Iterate through permutations in initial set, and create a new set of permutations to add to results
            while (itr.hasNext()) {
                Permutator next = new Permutator(itr.next().permutation.getPerm());
                ResultMap res = joe(next);
                thisOrder.combine(res);
            }

            // Ressign start order for next permutation
            startOrder = thisOrder;
        }

        return startOrder;
    }

    /**
     * Run the joe function in its own thread. Should be useful when running arrays larger than size 10.
     */
    static class ThreadedJoe implements Runnable {

        public ResultMap map;
        ArrayList<Integer> init;

        ThreadedJoe(ArrayList<Integer> init) {
            this.init = init;
        }

        public void run() {
            this.map = joe(init);
        }
    }

}
