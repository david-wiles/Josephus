package perm;


import java.io.*;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Map to keep track of permutation results.  The key used is the lexicographical representation of the permutation and
 * the value is the result object created by the permutation.  The result object should be used to store additional
 * information about the permutation, since there should only be one entry for each permutation
 */
public class ResultMap implements Iterable<ResultMap.Result> {

    private HashMap<String, Result> map;

    public ResultMap() {
        map = new HashMap<>();
    }

    public int size() {
        return map.size();
    }

    public HashMap<String, Result> getMap() {
        return this.map;
    }

    /**
     * Add a result object to the map. If the key already exists, then the existing Result is updated
     * @param permutation    Result object to be added
     */
    public void add(Permutation permutation) {
        String key = permutation.getEncoding();
        if (exists(key))
            map.get(key).occurrences++;
         else
            map.put(key, new Result(permutation));
    }

    /**
     * Only add a result to the map if the permutation is unique.
     * @param permutation    The result to add
     * @return          True if the result was added (unique permutation) false if it was not (duplicate)
     */
    public boolean addUnique(Permutation permutation) {
        String key = permutation.getEncoding();
        if (exists(key))
            return false;
        map.put(key, new Result(permutation));
        return true;
    }

    /**
     * Get a Result from a key. The key should be the lexicographic representation of the desired array
     * @param key   Lexicographic representation of the permutation
     * @return      The Result, if it was found
     */
    public Result get(String key) {
        return map.get(key);
    }

    /**
     * Check if a certain Result exists in the map
     * @param key   Encoded result
     * @return      True if the result exists, false otherwise
     */
    public boolean exists(String key) {
        return map.containsKey(key);
    }

    /**
     * Print all of the results in the map to the console, with the following format
     *
     * [key]    [skip number]   [result as array]
     */
    public void printResults() {
        map.forEach((key, res) -> System.out.println(
                key + "\t" +
                res.permutation.getSkipNumber() + "\t" +
                res.permutation.printFormattedArray()
        ));
    }

    /**
     * Create a new result file in the default location, with the name result_[milliseconds].txt
     */
    public void saveToFile() {
        File file = new File("result_" + System.currentTimeMillis() + ".txt");
        try {
            if (file.createNewFile()) {
                saveToFile(file);
            } else {
                System.out.println("Could not create new file, results not saved.");
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }

    }

    /**
     * Write the contents of the map into a specified location
     * @param file  The file object to save the results into
     * @throws FileNotFoundException    If the file passed to the method is not valid
     */
    public void saveToFile(File file) throws FileNotFoundException {
        try {
            final PrintWriter writer = new PrintWriter(file, "UTF-8");
            map.forEach((key, res) -> writer.println(
                    key + "\t" +
                    res.permutation.getSkipNumber() + "\t" +
                    res.permutation.printFormattedArray()
            ));
            writer.close();
        } catch (UnsupportedEncodingException ex) {
            System.out.println(ex.getMessage());
        }
    }

    /**
     * Get the iterator for the values in the map
     * @return  Iterator of result objects in this map
     */
    public Iterator<Result> iterator() {
        return map.values().iterator();
    }

    /**
     * Combine two ResultMaps into a single map
     * @param other The ResultMap to flatten into this one
     */
    public synchronized void combine(ResultMap other) {
        Iterator<Result> itr = other.iterator();
        while (itr.hasNext()) {
            this.add(itr.next().permutation);
        }
    }

    /**
     * Combine two ResultMaps into a single map without duplicates
     * @param other The ResultMap to flatten into this one
     */
    public void combineUnique(ResultMap other) {
        Iterator<Result> itr = other.iterator();
        while (itr.hasNext()) {
            this.addUnique(itr.next().permutation);
        }
    }

    /**
     * Create a copy of this object
     * @return  A pointer to a new ResultMap
     */
    public ResultMap copy() {
        ResultMap copy = new ResultMap();
        for (Result result : this)
            copy.add(result.permutation);
        return copy;
    }

    /**
     * Struct to used to store a permutation in the ResultMap along with other information about the permutation.
     */
    public static class Result {

        public Permutation permutation;
        public int occurrences;

        public Result(Permutation permutation) {
            this.permutation = permutation;
            this.occurrences = 1;
        }

    }
}
