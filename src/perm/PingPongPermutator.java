package perm;

import java.util.Collection;

public class PingPongPermutator extends Permutator {

    public PingPongPermutator(Collection<Integer> array) {
        super(array);
    }

    public Permutation permutate(Integer skip) {
        boolean forwards = true;

        while (!queue.isEmpty()) {
            nextPermutation(skip, forwards);
            forwards = !forwards;
        }
        try {
            return queue.getResult(skip);
        } catch (PermutationUnfinished err) {
            System.out.println(err.getMessage());
            return null;
        }
    }

    protected void nextPermutation(Integer skip, boolean direction) {
        int index;
        if (direction)
            index = (start + (skip - 1)) % queue.size();
        else
            index = (queue.size() - 1 - (start - ((skip % queue.size()) - 1))) % queue.size();

        start = queue.nextIndex(index);
        queue.move(index);
    }

}
