package perm;

import java.math.BigInteger;
import java.util.ArrayList;

import static util.Utilities.FACTORIALS;
import static util.Utilities.calculateEncoding;


/**
 * Object to contain information about a specific Josephus permutation result as well as generate a lexicographical key
 */
public class Permutation {

    private String encoding;
    private int permSize;
    private int skipNumber;

    /**
     * Create a result object from an integer array
     *
     * @param arr   Array to store in the result
     */
    public Permutation(ArrayList<Integer> arr) {
        permSize = arr.size();
        encoding = calculateEncoding(arr, permSize);
    }

    public Permutation(ArrayList<Integer> arr, int skipNumber) {
        this.skipNumber = skipNumber;
        permSize = arr.size();
        encoding = calculateEncoding(arr, permSize);
    }

    public Permutation(String encoding, int permSize, int skipNumber) {
        this.encoding = encoding;
        this.permSize = permSize;
        this.skipNumber = skipNumber;
    }


    // Accessors
    /**
     * Get the permutation as an array in order to use as input for another permutation
     *
     * @return  Permutation as an integer array
     */
    public ArrayList<Integer> getPerm() {

        ArrayList<Integer> permutation = new ArrayList<>(permSize);

        int[] seq = new int[permSize];
        int length = permSize;
        BigInteger x;
        BigInteger code = new BigInteger(encoding);

        for (int i = 0; i < permSize;)
            seq[i] = ++i;

        for (int i = 0; i < permSize - 1; ++i) {
            x = code.divide(FACTORIALS[length - 1]);
            permutation.add(seq[x.intValue()]);

            for (int j = x.intValue(); j < length - 1;)
                seq[j] = seq[++j];

            code = code.mod(FACTORIALS[length - 1]);
            length--;
        }

        permutation.add(permSize - 1, seq[0]);

        return permutation;
    }

    public String getEncoding() {
        return encoding;
    }

    public int getSkipNumber() {
        return skipNumber;
    }

    /**
     * Return the array, formatted to be printed to the console
     *
     * @return  Array represented as a string.
     */
    public String printFormattedArray() {
        StringBuilder builder = new StringBuilder();
        builder.append("[ ");

        for (Integer val: getPerm())
            builder.append(val.toString()).append(" ");

        builder.append("]");
        return builder.toString();
    }

    @Override
    public String toString() {return this.getPerm().toString(); }

}
