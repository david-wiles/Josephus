package perm;


import util.Utilities;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Get the permutation for an object by calculating the cross product instead of manually producing the
 * permutation
 */
public class CrossProductPermutator extends Permutator {

    ArrayList<Integer> base_perm;

    public CrossProductPermutator(Collection<Integer> array) {
        super(array);
        base_perm.addAll(array);
    }

//    public Permutation permutate(Integer skip) {
//        Utilities.cross(base_perm, );
//    }
}
