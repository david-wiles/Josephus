package perm;

import edu.Josephus.Josephus;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * Implementation of a basic (first-order) permutation. An array can be passed (from the result of another permutation)
 * or an integer indicating the array length can be passed for a fresh permutation.
 *
 * This class utilizes the Josephus queue as a data structure for an efficient implementation of the permutation
 */
public class Permutator {

    protected JosephusQueue queue;
    protected int start = 0;
    protected int init_size = 0;

    /**
     * Initialize the permutation from an existing array
     *
     * @param array The array to create the permutation from
     */
    public Permutator(Collection<Integer> array) {
        queue = new JosephusQueue(array);
        init_size = array.size();
    }

    public Permutator(Collection<Integer> array, int numLives) {
        queue = new FelineJosephusQueue(array, numLives);
        init_size = array.size();
    }

    public int size() {
        return init_size;
    }

    /**
     * Execute the Josephus permutation on the array. Repeatedly calls nextPermutation until the input queue is empty.
     *
     * @param skip  The size of the skip for the permutation.
     * @return      A result object containing the resultant array.
     */
    public Permutation permutate(Integer skip) {
        // Ensure that a previous permutation queue is reset
        queue.reset();

        while (!queue.isEmpty())
            nextPermutation(skip);

        try {
            return queue.getResult(skip);
        } catch (PermutationUnfinished err) {
            System.out.println(err.getMessage());
            return null;
        }
    }

    /**
     * Function which executes a single permutation by finding the index of the next element to remove.
     *
     * @param skip  The skip number of the permutation
     */
    protected void nextPermutation(Integer skip) {
        int index = (start + (skip - 1)) % queue.size();
        start = queue.nextIndex(index);
        queue.move(index);
    }

    /**
     * Object intended to make manipulating a Josephus permutation easier.  The object contains an input array and an
     * output queue. The permutation class should call this class's move function, which moves an element from that index
     * of the input array into the output queue.  This class also maintains the size of the input and output structs
     */
    static class JosephusQueue {

        ArrayList<Integer> in;
        ArrayList<Integer> out;
        ArrayList<Integer> init;

        /**
         * Initialize queues from a collection, where inArray is full and
         * outArray is empty
         *
         * @param collection    Collection to iterate through
         */
        JosephusQueue(Collection<Integer> collection) {
            int i = 0;
            int size = collection.size();
            in = new ArrayList<>(size);
            out = new ArrayList<>(0);
            Iterator<Integer> itr = collection.iterator();

            while (i < size && itr.hasNext()) {
                int element = itr.next();
                init.add(element);
                in.add(element);
            }
        }

        void reset() {
            if (!out.isEmpty()) {
                out.clear();
                in.clear();
                in.addAll(init);
            }
        }

        /**
         * Move an element at the given index from inArray to outArray
         *
         * @param index     The index to move an item from
         */
        void move(int index) {
            out.add(in.remove(index));
        }

        int nextIndex(int index) {
            return index;
        }

        /**
         * Return the current value of inSize, which indicates how many values remain in inArray
         *
         * @return  Size of inArray (1 more than current max index)
         */
        int size() {
            return in.size();
        }

        /**
         * Get the output array
         *
         * @return  outArray
         */
        public ArrayList<Integer> getPermutationArray() {
            return out;
        }

        /**
         * Transform output array into a string and return
         *
         * @return  Each element of outArray concatenated into a string
         */
        public String getPermutationKey() {
            StringBuilder key = new StringBuilder();
            for (Integer val: out)
                key.append(val);
            return key.toString();
        }

        /**
         * Get an instance of a result object from this queue
         *
         * @return  Result constructed from this queue
         * @throws PermutationUnfinished    Error thrown if input array is not empty
         */
        public Permutation getResult() throws PermutationUnfinished {
            if (in.size() == 0)
                return new Permutation(out);
            else
                throw new PermutationUnfinished(in.toString());
        }

        /**
         * Return the result with the skip number used to generate it
         *
         * @param skip  Skip number to assign
         * @return      Result object
         * @throws      PermutationUnfinished if the input array isn't empty
         */
        Permutation getResult(int skip) throws PermutationUnfinished {
            if (in.size() == 0)
                return new Permutation(out, skip);
            else
                throw new PermutationUnfinished(in.toString());
        }

        /**
         * Returns a boolean indicating if the inArray is empty, that is, there is nothing left to permutate
         *
         * @return  State of the inArray (empty or not)
         */
        boolean isEmpty() {
            return in.size() == 0;
        }

    }

    /**
     * Variation on a regular permutation in which an item must be chosen a set number of times before being removed
     * from the queue. This is implemented by adding a decorator to a JosephusQueue which will only move an item if
     * it is out of 'lives'
     */
    static class FelineJosephusQueue extends JosephusQueue {

        ArrayList<Integer> lives;

        public FelineJosephusQueue(Collection<Integer> collection, int numLives) {
            super(collection);
            this.lives = new ArrayList<>(this.size());
            for (int i = 0; i < this.size(); i++) {
                lives.add(numLives);
            }
        }

        @Override
        void move(int index) {
            int elementIndex = in.get(index) - 1;
            int livesLeft = lives.get(elementIndex) - 1;
            lives.set(elementIndex, livesLeft);
            if (livesLeft == 0) {
                super.move(index);
            }
        }

        @Override
        int nextIndex(int index) {
            // If the removed element was at the end of the list,
            // the input array will be shorted than the index
            if (in.size() <= index)
                return index;

            // If the number of lives is one before the move, then the
            // element should be removed (but this is not the best way to do it ...
            if (lives.get(in.get(index) - 1) == 1) {
                return index;
            } else {
                return index + 1;
            }
        }

    }

    /**
     * Exception thrown in the case of a permutation returning while the input array is not empty
     */
    static class PermutationUnfinished extends Exception {

        PermutationUnfinished(String message) {
            super("Permutation unfinished: inArray size is not zero. " + message);
        }

    }

}
