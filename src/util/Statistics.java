package util;

import perm.Permutation;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;

public abstract class Statistics {

    /**
     * Returns a string displaying all calculatable statistics for a permutation.
     *
     * @param p The permutation to generate statistics for
     * @return  A String containing the statistics
     */
    public static String all(Permutation p) {

        return  "Permutation: " + p + "\n" +
                "Pairity:     " + parity(p) + "\n" +
                "Circular:    " + Arrays.toString(circular(p)) + "\n" +
                "Order:       " + order(p) + "\n" +
                "Lehmer Code: " + Arrays.toString(lehmerCode(p)) + "\n" +
                "InvTable:    " + Arrays.toString(inversionTable(p)) + "\n";
    }

    /**
     * Get the even / odd parity of a given permutation
     *
     * @param array Set to find pairity of
     * @return      1 for odd pairity, 0 for even pairity
     */
    public static int parity(ArrayList<Integer> array) {
        int res = 0;

        int index = 0;
        for (Integer itr: array) {
            for (int i = index++; i < array.size(); ++i) {
                if (itr > array.get(i))
                    res++;
            }
        }

        if (res % 2 == 0)
            return 1;
        return -1;
    }

    public static int parity(Permutation p) {
        return parity(p.getPerm());
    }

    /**
     * Represent a permutation as a circular permutation by setting its first element to 1. Comparision between
     * permutations ordered in this fashion will show whether the circular representation of two permutations are equal
     *
     * @param perm  Permutation to generate circular representation of (as an arraylist)
     * @return      ArrayList containing circular permutation
     */
    public static Integer[] circular(ArrayList<Integer> perm) {
        Integer[] res = new Integer[perm.size()];
        int oneIndex = perm.indexOf(1);
        int idx = 0;

        for (int i = oneIndex; i < perm.size(); ++i)
            res[idx++] = perm.get(i);
        for (int i = 0; i < oneIndex; ++i)
            res[idx++] = perm.get(i);

        return res;
    }

    public static Integer[] circular(Permutation p) {
        return circular(p.getPerm());
    }

    /**
     * Calculate the number of permutations needed to return to the identity permutation
     *
     * @param perm  Permutation array to find order of
     * @return      int order
     */
    public static int order(ArrayList<Integer> perm) {
        ArrayList<Integer> pCopy = perm;
        ArrayList<Integer> id = new ArrayList<>(perm.size());
        int order = 0;

        for (int i = 0; i < perm.size();)
            id.add(++i);
        while (!pCopy.equals(id)) {
            pCopy = Utilities.cross(pCopy, perm);
            ++order;

            // Break if order is unreachable
            if (order > 10000)
                break;
        }

        return order;
    }

    public static int order(Permutation p) {
        return order(p.getPerm());
    }

    /**
     * Calculate the lehmer codes for a particular permutation. Indicates the number of inversions on a specific
     * element, in other words, the number of inversions after that point in the permutation
     *
     * @param perm  Array of a permutation to calculate lehmer code for
     * @return      ArrayList of lehmer codes
     */
    public static Integer[] lehmerCode(ArrayList<Integer> perm) {
        Integer[] codes = new Integer[perm.size()];

        for (int i = perm.size() - 1; i >= 0; --i) {
            int code = 0;
            for (int j = i; j < perm.size(); ++j) {
                if (perm.get(j) < perm.get(i))
                    code++;
            }

            codes[i] = code;
        }

        return codes;
    }

    public static Integer[] lehmerCode(Permutation p) {
        return lehmerCode(p.getPerm());
    }

    /**
     * Similar to a lehmer code, but shows the number of times an element is inverted. In other words, the number of
     * times an element is the inverted element.
     *
     * @param perm  The permutation to generate an inversion table for
     * @return      The inversion table
     */
    public static Integer[] inversionTable(ArrayList<Integer> perm) {
        Integer[] table = new Integer[perm.size()];

        for (int i = 0; i < perm.size(); ++i) {
            int invNum = 0;
            for (int j = i; j >= 0; --j) {
                if (perm.get(j) < perm.get(i))
                    invNum++;
            }
            table[i] = invNum;
        }

        return table;
    }

    public static Integer[] inversionTable(Permutation p) {
        return inversionTable(p.getPerm());
    }

}
