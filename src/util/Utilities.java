package util;

import perm.ResultMap;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.function.Function;

/**
 * Class containing only static methods which are useful in other project files
 */
public abstract class Utilities {

    // Array containing pre-calculated factorials for lexicographical encoding
    public static final BigInteger[] FACTORIALS = {
            new BigInteger("1"),                //0
            new BigInteger("1"),                // 1
            new BigInteger("2"),                // 2
            new BigInteger("6"),                // 3
            new BigInteger("24"),               // 4
            new BigInteger("120"),              // 5
            new BigInteger("720"),              // 6
            new BigInteger("5040"),             // 7
            new BigInteger("40320"),            // 8
            new BigInteger("362880"),           // 9
            new BigInteger("3628800"),          // 10
            new BigInteger("39916800"),         // 11
            new BigInteger("479001600"),        // 12
            new BigInteger("6227020800"),       // 13
            new BigInteger("87178291200")};     // 14

    /**
     * Isaiah's implementation of least common multiple
     * Get the least common multiple for a given integer number
     *
     * @param n Integer to find the least common multiple of
     * @return  BigInteger representation of lcm
     */
    public static BigInteger lcm(Integer n) {
        ArrayList<BigInteger> lcmFactors = new ArrayList<>();
        BigInteger lcm = BigInteger.valueOf(1); //to always initialize LCM at 1
        if (n <= 0) {
            System.out.println("Invalid Parameter");
            System.exit(-1);
        }
        for (Integer i = 0; i <= n; i++) {
            BigInteger factor = BigInteger.valueOf(i);
            for (Integer j = 0; j < lcmFactors.size(); j++) {
                if (factor.mod(lcmFactors.get(j)).compareTo(BigInteger.valueOf(0)) == 0) { //confusing looking, but this is just the ith number mod the jth element of lcmFactors
                    //if current factor is divisible by the current factors, divide it.
                    factor = factor.divide(lcmFactors.get(j));
                }
            }
            if ((factor.compareTo(BigInteger.valueOf(1))) == 1) { //a compareTo value of 1 indicates factor is strictly greater than 1
                //After being divided by all the terms in the current prime factorization of the LCM, whatever is left is added to the array if it is greater than 1
                lcmFactors.add(factor);
            }
        }

        for (Integer k = 0; k < lcmFactors.size(); k++) { //after prime factorization is complete, simply multiply all elements of the array together.
            lcm = lcm.multiply(lcmFactors.get(k));
        }

        return lcm;
    }

    /**
     * Get the cross product of two sets
     *
     * @param lhs   Left set
     * @param rhs   Right set
     * @return      Cross product
     */
    public static ArrayList<Integer> cross(ArrayList<Integer> lhs, ArrayList<Integer> rhs) {
        ArrayList<Integer> res = new ArrayList<>(lhs.size());
        for (Integer index : lhs)
            res.add(rhs.get(index - 1));
        return res;
    }

    /**
     * Get the permutation associated with a particular lexicographic encoding
     *
     * @param encoding  The encoding to decode, as a string
     * @param permSize  The size of the permutation to decode
     * @return          ArrayList representation of the permutation
     */
    public static ArrayList<Integer> makeArray(String encoding, int permSize) {

        ArrayList<Integer> permutation = new ArrayList<>(permSize);

        int[] seq = new int[permSize];
        int length = permSize;
        BigInteger x;
        BigInteger code = new BigInteger(encoding);

        for (int i = 0; i < permSize;)
            seq[i] = ++i;

        for (int i = 0; i < permSize - 1; ++i) {
            x = code.divide(FACTORIALS[length - 1]);
            permutation.add(seq[x.intValue()]);

            for (int j = x.intValue(); j < length - 1;)
                seq[j] = seq[++j];

            code = code.mod(FACTORIALS[length - 1]);
            length--;
        }

        permutation.add(permSize - 1, seq[0]);

        return permutation;
    }

    /**
     * Generate a lexicographical key as a string.  This is used as the key in the ResultMap object.
     * This function is a direct implementation of the "encode" function from Dr. Gary's implementation.
     *
     * @return  String representation of the array's lexicographical number
     */
    public static String calculateEncoding(ArrayList<Integer> perm, int permSize) {

        int[] seq = new int[permSize];
        for (int i = 0; i < permSize;)
            seq[i] = ++i;

        BigInteger key = new BigInteger("0");
        int x, loc;
        int length = permSize;

        for (int i = 0; i < permSize - 1; ++i) {
            x = perm.get(i);
            loc = 0;
            while (seq[loc] != x) loc++;

            key = key.add(FACTORIALS[length - 1].multiply(new BigInteger(Integer.toString(loc))));

            for (int j = loc; j < length - 1;)
                seq[j] = seq[++j];

            length--;
        }

        return key.toString();
    }

    /**
     * Takes a ResultMap and a function, and uses that function to write a result to a file.
     * The function should take a Result object as a parameter and return a String
     * @param map   ResultMap to iterate and print
     * @param fn    Print function
     */
    public static void writeFunction(ResultMap map, Function<ResultMap.Result, String> fn) {
            try {
            File f = new File("result_" + System.currentTimeMillis() + ".txt");
            final PrintWriter writer = new PrintWriter(f, "UTF-8");
            map.forEach(res -> {
                String out = fn.apply(res);
                writer.println(out);
            });
            writer.close();
        } catch (IOException err) {
            System.err.print("Could not save results: ");
            System.err.println(err.getMessage());
        }
    }

}
