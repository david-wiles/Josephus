BEGIN {
    for ( i = 1; i <= 720; i++)
        arr[i] = 0
}
{
    arr[$1] = 1
}
END {
    for ( x in arr ) {
        if (arr[x] == 0) {
            print x
        }
    }
}
